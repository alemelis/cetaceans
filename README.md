# cetaceans

[![alt](https://cdn.dribbble.com/users/124845/screenshots/855140/whale400x300.gif)](https://dribbble.com/shots/855140-whale-gif-animated)

### Build as
```bash
cd container-folder
docker build -t alemelis/container-tagname .
```

### or simpy pull as
```bash
docker pull alemelis/container-tagname
```

### then run as
```bash

```

## Dockerfiles

- AlexeyAB/darknet: [alemelis/darknet](https://cloud.docker.com/repository/docker/alemelis/darknet)

